# Important Git Command
## Git workflow
![Git workflow](./images/git_workflow.png)
## Clone repo
```
git clone <repository link>
```

## Add files
```
git add <link to file>
```

## Create a commit 
```
git commit -m "Message"
```

## Push commits to remote repository
```
git push
```

## Fetch data from remote repository
```
git fetch
```

## Create a new branch
```
git checkout -b <new branch name>
```

## Merge branch
```
git merge <branch name>
git merge --abort
```
## Stash 
```
git stash
git stash pop
```

## Reset
This command is **not deleted untracked files**
```
git reset --hard  # *Not delete untracked files*
```

## Clean
This command is **fully deleted untracked files**
```
git clean -xdf
-x means remove ignored files
-f means clean untracked files
-d means remove directories
```


